<!DOCTYPE HTML>
<html>
<head>
	<script src="js/jquery-1.7.1.min.js"></script>
	<script src="js/dropzone.js"></script>
	<script src="js/jquery-ui.js"></script>
	<script src="js/fabric/dist/fabric.js"></script>

	<style>
	.dz-preview, .dz-file-preview, .dz-processing, .dz-error, .dz-details, .dz-progress,
    .dz-success-mark, .dz-error-mark, .dz-error-message{
      display: none;
    }
	/*Fonts manipulations*/
	@font-face{
		font-family: OxygenLight;
		src:url(fonts/Oxygen-Light.ttf);
	}
	@font-face{
		font-family: OxygenBold;
		src:url(fonts/Oxygen-Bold.ttf);
	}
	@font-face{
		font-family: OxygenRegular;
		src:url(fonts/Oxygen-Regular.ttf);
	}
	@font-face{
		font-family: Cargo;
		src:url(fonts/8th-Cargo.ttf);
	}
	.OxygenLight{
		font-family: OxygenLight;
		font-size: 18px;
		font-weight: lighter;
	}
	.OxygenBold{
		font-family: OxygenBold;
		font-weight: bold;
		font-size: 18px;
	}
	.OxygenRegular{
		font-family: OxygenRegular;
		font-size: 18px;
		font-weight: normal;
	}
	.Cargo{
		font-family: Cargo;
		font-size: 18px;
	}
	/*----------------------------*/
	body{
		padding-right: 20px;
		padding-left: 20px;
		background: white;
		z-index: 1;
		overflow-x: hidden; 
	}
	.modifications p{
		margin-left: 30px;
	}
	#text-styling p{
		margin-left: 0px;
	}
	p{
		margin: 5px;
	}
	input[type="radio"] {
    -webkit-appearance: checkbox; /* Chrome, Safari, Opera */
    -moz-appearance: checkbox;    /* Firefox */
    -ms-appearance: checkbox;   
	}
	#color-picker p{
		margin-bottom: 5px;
	}
	label input{
		display: none;
	}
	label{
		margin-top: 5px;
		margin-left: 8px;
		height: 20px;
		width: 20px;
	}
	label img{
		height: 15px;
		width: 15px;
		display: inline-block;
		margin-top: 5px;
		padding: 0px;
	}
	label input:checked +img{
		border: 1px solid blue;
	}
	input[type="email"]{
		margin-top: 10px;
		width: 500px;
		height:40px;
		font-size: 26px;
	}
	#sendEmail{
		margin-bottom: 15px;
		height: 40px;
		width: 60px;
	}
	.image-combinor{
		float: left;
		margin: 0 auto;

		width: 50%;
		min-height: 510px;
	}
	#canvas{
		margin: auto;
		width: 96%;
		height: 580px;
		border: 1px solid grey;
	}
	.right-side{		
		float: right;
		margin: 0 auto;
		padding-top: 10px;
		padding-right: 5px;
		padding-left: 20px;
		width: 45%;
		min-height: 510px;
	}
	.select-from-library{
		float: left;
		margin: 0 auto;
		width: 45%;
		min-height: 500px;

	}
	.library-file-selector{
		margin-top: 10px;
		/*min-width: 260px;*/
		width: 100%;
		min-height: 480px;
		max-height: 480px;
		border: 1px solid grey;
		overflow-y: auto;
		text-align: center;
	}
	.library-file-selector img{
		width: 240px;
		margin-left: 5px;
		z-index: -1;
	}
	.modifications{
		float: right;
		margin: 0 auto;
		margin-left: 10px;
		width: 50%;
		min-height: 500px;
	}
	.upload-files{
		margin: 10px auto;
		border: 1px solid grey;
		width: 90%;
		min-height: 180px;
	}
	.buttonDiv{
		margin: auto;
		text-align: center;
	}
	/*Buttons*/
	#uploadAll{
		width: 60%;
		height: 30px;
		font-size: 16px;
		font-weight: bold;
	}
	#generate{
		width: 60%;
		height: 30px;
		font-size: 16px;
		font-weight: bold;
	}
	#sendEmail{
		font-size: 16px;
		font-weight: bold;
	}
	/*------*/
	.text-editor{
		text-align: center;
	}
	#input-text-view{
		margin: auto;
		padding: 20px;
		background: white;
		border: 1px solid grey;
		height: 40px;
		width: 78%;
		vertical-align: middle;
	}
	#input-text-view p{
		margin-left: 0px;
	}
	#text-styling{
		padding-top: 10px;
		margin: auto;
		
	}
	#text-styling p{
		font-size: 14px;
	}
	.sending{
		float: left;
		margin-left: 7%;
		margin-top: 10px;
		width: 100%;
		clear: both;
		min-height: 50px;
		vertical-align: middle;
	}
	.email-input{

		float: left;
	}
	#email-sending{
		margin-left: 15px;
		margin-top: 13px;
		float: left;
	}
	</style>
</head>
<body>

	<div class="image-combinor">
		<div>
		<p style="display:block; margin-bottom:20px;">Canvas - Drag and Drop files here:</p>	
		</div>
		<canvas id="canvas" width="550" height="480">
			
		</canvas>
	</div>

	<div class="right-side">
		<div class="select-from-library">
			<div><p>Select from library:</p></div>
			<div class="library-file-selector">
				<img src="library/background.png">
				<img src="library/light.png">
				<img src="library/robot1.png">
				<img src="library/robot2.png">
				<img src="library/robot3.png">
				<img src="library/robot4.png">
			</div>	
			</div>

		<div class="modifications">
			<p>Upload:</p>
			<div class="upload-files">
			
			</div>

			<div class="buttonDiv">
				<button id="uploadAll" onclick="loadAll()">Upload</button>
			</div>

			<p>Text:</p>
			<div class="text-editor">
				
				<div id="input-text-view"><label id="textView"></label></div>
				</div>
				<div id="text-styling">
					<p>Font type:
					<select name="font">
					<option value="OxygenLight">Oxygen Light</option>
					<option value="OxygenBold">Oxygen Bold</option>
					<option value="OxygenRegular">Oxygen Regular</option>
					<option value="Cargo">8th Cargo</option>
					</select></p>

					<p>Text: <input type="text" id="setText"></p>
					<form id="color-picker">
					<p>Color: 
					<label>
					<input type="radio" name="color" value="black" checked>
					<img src="img/black.png" />
					</label>
					<label>
					<input type="radio" name="color" value="#840E00">
					<img src="img/brown.png" />
					</label>
					<label>
					<input type="radio" name="color" value="yellow">
					<img src="img/yellow.png" />
					<label></p>
					</form>
				</div>
				<div class="buttonDiv">
					<button id="generate" onclick="generateText()">Generate</button>
			</div>
		</div>
	</div>

	<div class="sending">

		<div class="email-input">
		<input id="email" type="email" placeholder="Enter e-mail address here">
		</div>

		<div id="email-sending">
		<button id="sendEmail" onclick="send()">Send</button>
		</div>
	</div>

</body>
<script>
var canvas = new fabric.Canvas('canvas');
libraryFix();


$(".library-file-selector img").draggable({
    helper:'clone',
});

$("#canvas").droppable({
    drop:dragDrop,
});


function dragDrop(e,ui){
    var draggable = ui.draggable;
    console.log(draggable.attr('src'));
    fabric.Image.fromURL(draggable.attr('src'), function(oImg){
    	oImg.scaleY = 0.5;
    	oImg.scaleX = 0.5;
    canvas.add(oImg);
    });
}

function generateText(){
	$('#textView').html($('#setText').val());
	$('#textView').removeClass("OxygenLight");
	$('#textView').removeClass("OxygenBold");
	$('#textView').removeClass("OxygenRegular");
	$('#textView').removeClass("Cargo");
	$('#textView').addClass($("[name='font']").attr('value'));
	console.log($("[name='color']:checked").attr('value'));
	$('#textView').css("color",$("[name='color']:checked").attr('value'));

	if($("[name='font']").attr('value')=="OxygenBold"){
		var text = new fabric.Text($('#setText').val(),{
		fill: $("[name='color']:checked").attr('value'),
		fontFamily: $("[name='font']").attr('value'),
		fontWeight:'bold'});
	}
	else {
	var text = new fabric.Text($('#setText').val(),{
		fill: $("[name='color']:checked").attr('value'),
		fontFamily: $("[name='font']").attr('value'),
	});
	}
	canvas.add(text);

}	

$('#rotate').click(function(){
   if ($('#canvas').attr('width')==620) {
   	$('#canvas').remove();
   	$('.main-pattern').append('<canvas id="canvas" width="400" height="620"></canvas>');
   } else if ($('#canvas').attr('width')==400){
   	$('#canvas').remove();
   	$('.main-pattern').append('<canvas id="canvas" width="620" height="400"></canvas>');
   } 
});
function libraryFix(){
	var text = new fabric.Text('Hi',{
		fontFamily: 'Cargo'
	});
	canvas.add(text);
	text.remove();

	var text = new fabric.Text('Hi',{
		fontFamily: 'OxygenBold'
	});
	canvas.add(text);
	text.remove();

	var text = new fabric.Text('Hi',{
		fontFamily: 'OxygenRegular'
	});
	canvas.add(text);
	text.remove();

	var text = new fabric.Text('Hi',{
		fontFamily: 'OxygenLight'
	});
	canvas.add(text);
	text.remove();
}

var dropzone = new Dropzone(".upload-files", {
  url: 'phpscripts/upload.php',
  maxFiles: 10,
  parallelUploads: 10,
  uploadMultiple: true,
  autoQueue:true,
  autoProcessQueue: false,
});
dropzone.on("addedfile", function(file) {
	$('.upload-files').append("<p>"+file.name+"</p>");

});
function loadAll(){
    dropzone.processQueue();
}   

dropzone.on("success", function(file){
    $("<img src=\"uploaded/"+file.name+"\">").insertBefore("img:first");
    $(".library-file-selector img").draggable({
    helper:'clone',
});
});
function send(){
	canvas.deactivateAllWithDispatch();
	var dataURL = canvas.toDataURL();
	$.ajax({
  		type: "POST",
  		url: "phpscripts/save.php",
  		data: { 
    		 imgBase64: dataURL,
    		 email: $('#email').val()
  		}
	}).done(function(o) {
  	console.log('saved'); 
	});
}
</script>
</html>